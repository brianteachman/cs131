/*
 * Lab 4 - Grade Translator
 *
 * Write a C++ program that asks the user for a test score. Use a
 * series of if and else if statements that compare the score to
 * the standard grading scale of >= 90 for an A, 80-89 for a B, etc.
 *
 * Student: Brian Teachman (mr.teachman@gmail.com)
 * Date:    7/21/2016
 * Class:   CS& 131: Computer Science I C++
 * */

#include <iostream>

using namespace std;

char getLetterGrade(int);
// Receive's a number from main()and return a letter to main().

string getFineLetterGrade(int);
// Receive's a number from main()and return a letter (ie, A or A+) to main().

string getNumberRange(char);
// Receive's a letter from main()and return a number to main().

int main() {

    // allocate memory for three 8-bit integers
    int testScore;

    // ask user for three numbers
    cout << endl << "What was your score on that test?";
    cin >> testScore;

    char grade = getLetterGrade(testScore);
    cout << "Well, feel free to cushion it with a " << grade << ". "
         << "Which falls anywhere in the range of " << getNumberRange(grade) << "." << endl;

    cout << "But more specifically your looking at a " << getFineLetterGrade(testScore) << "." << endl;

    // check compiler macro
#if ( _MSC_VER ) // if using Visual Studio
    system("pause");
#endif

    return 0;
}

char getLetterGrade(int test_score) {
    char letterGrade;
    if (test_score <= 100 & test_score >= 90)
        letterGrade = 'A';
    else if (test_score < 90 && test_score >= 80)
        letterGrade = 'B';
    else if (test_score < 80 && test_score >= 70)
        letterGrade = 'C';
    else if (test_score < 70 && test_score >= 60)
        letterGrade = 'D';
    else
        letterGrade = 'F';
    return letterGrade;
}

string getFineLetterGrade(int test_score) {
    string letterGrade;
    if (test_score <= 100 & test_score >= 95)
        letterGrade = "A+";
    else if (test_score < 95 & test_score >= 90)
        letterGrade = "A";
    else if (test_score < 90 && test_score >= 85)
        letterGrade = "B+";
    else if (test_score < 85 && test_score >= 80)
        letterGrade = "B";
    else if (test_score < 80 && test_score >= 75)
        letterGrade = "C";
    else if (test_score < 75 && test_score >= 70)
        letterGrade = "C+";
    else if (test_score < 70 && test_score >= 60)
        letterGrade = "D";
    else
        letterGrade = "F";
    return letterGrade;
}

string getNumberRange(char test_grade) {
    string letterGrade;
    switch(test_grade) {
        case 'A':
            letterGrade = "90-100";
            break;
        case 'B':
            letterGrade = "80-89";
            break;
        case 'C':
            letterGrade = "70-79";
            break;
        case 'D':
            letterGrade = "60-69";
            break;
        default:
            letterGrade = "0 to 69...";
            break;
    }
    return letterGrade;
}
