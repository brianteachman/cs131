/*
 * Lab 5: Provide change
 *
 * Write a program that takes a number of cents from the user and outputs
 * the smallest number of coins made up of quarters, dimes, nickels and
 * pennies that equal the value of total cents received. The conversion
 * should be made by a call by reference function that uses the variables
 * quarters, dimes, nickels and pennies.
 *
 * If you would like to do something more interesting for extra credit then
 * try building a cash register calculator that takes price from the user
 * and cash tendered from the user and uses a call by reference function to
 * calculate change in dollars, quarters, nickels and pennies.
 *
 * Student: Brian Teachman (mr.teachman@gmail.com)
 * Date:    7/28/2016
 * Class:   CS& 131: Computer Science I C++
 */
#include <iostream>
#include <string>
#include <cstdlib> // ^ exit(1)
#include <sstream>

using namespace std;

int getInput(string query_for_cents);
//

int recieveTender(string tender_as_cents);
//

void makeChange(int& tender_as_cents);
//

void displayChange(int quarters_returned,
                   int dimes_returned,
                   int nickels_returned,
                   int pennies_returned);

void checkOutPayment(int price_in_cents);
//

// ----------------------------------------------------------------------------


int main() {

    checkOutPayment(163);
//    recieveTender("163");
    return 0;
}


// ----------------------------------------------------------------------------

void makeChange(int& tender_as_cents) {
    int quarters=0, dimes=0, nickels=0, pennies=0;
    while (tender_as_cents > 0) {

        if (tender_as_cents > 25) { //quarters
            quarters = (tender_as_cents / 25);
            tender_as_cents = tender_as_cents % 25;
        }
        else if (tender_as_cents > 10) { //dimes
            dimes = tender_as_cents / 10;
            tender_as_cents = tender_as_cents % 10;
        }
        else if (tender_as_cents > 5) { //nickels
            nickels = tender_as_cents / 5;
            tender_as_cents = tender_as_cents % 5;
        }
        else { //pennies
            pennies = tender_as_cents;
            tender_as_cents = 0;
        }
    }
    displayChange(quarters, dimes, nickels, pennies);

}

void checkOutPayment(int price_in_cents) {
//    int moneyTendered = getInput("That will be $1.63 please: (enter amount to pay in cents)");
    int moneyTendered = recieveTender("163"); //to_string(price_in_cents) doesn't work ...


//    string price_char = to_string(price_in_cents);  // to_string is NOT defined in CLion!
//and
//    char *price_char = itoa(price_in_cents);        // itoa is NOT defined in CLion!
//    string price = string(price_char);
    int change_owed = moneyTendered - price_in_cents; // atoi converts string to int

//    int change_owed = moneyTendered - atoi(price.c_str()); // atoi converts string to int
//    int change_owed = moneyTendered - price_int; // atoi converts string to int
//    int change_owed = moneyTendered - atoi(price.data()); // atoi converts string to int
    cout << "So, I owe you " << change_owed << " cents." << endl;

//    makeChange(moneyTendered);
    makeChange(change_owed);
}

// int age = getInput("That'll be $1.63 please: (enter amount in cents");
int getInput(string query_for_cents) {
    int num_of_cents;
    cout << query_for_cents;
    cin >> num_of_cents;
    return num_of_cents; // returns amount tendered in cents
}

int recieveTender(string price) {
    char input;
    string query = "That will be $" + price + " please: (enter amount to pay in cents)";
    int moneyTendered = getInput(query);
//    int moneyTendered = getInput("That will be $1.63 please: (enter amount to pay in cents)");
    cout << "OK, you tendered " << moneyTendered << " cents?" << endl
         << "(Press 'n' to re-enter, 'q' to cancel transaction, or any other key to continue)";
    cin >> input;
    while (input == 'N' || input == 'n')
    {
        moneyTendered = getInput(query);
    }
    if(input == 'q') exit(1);
    return moneyTendered;
}

void displayChange(int quarters_returned,
                   int dimes_returned,
                   int nickels_returned,
                   int pennies_returned) {
    cout << quarters_returned << " quarters, "
         << dimes_returned << " dimes, "
         << nickels_returned << " nickels, and "
         << pennies_returned << " pennies" << endl;
}