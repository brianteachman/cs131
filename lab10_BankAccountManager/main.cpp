/*
 * Brian Teachman <mr.teachman@gmail.com>
 * Date : 8/18/2016
 */
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

#include "input.h"

using namespace std;

bool DEV_MODE = true;
//bool DEV_MODE = false;

//-----------------------------------------------------------------------------

enum TransactionType { DEPOSIT, WITHDRAW, TRANSFER };

struct Transaction
{
    TransactionType type; // ie, if(type == DEPOSIT || type == WITHDRAW)
    double amount;
    int from_id;
    int to_id;
    // TODO: add datetime
};

int NUMBER_OF_ACCOUNTS = 0; // TODO: Fix this

class BankAccount
{
public:
    BankAccount(string account_name, double account_balance)
    {
        id = ++NUMBER_OF_ACCOUNTS;
        name = account_name;
        balance = account_balance;
        interest_rate = 0.05;
    }
    BankAccount(string account_name)
    {
        id = ++NUMBER_OF_ACCOUNTS;
        name = account_name;
        balance = 0;
        interest_rate = 0.05;
    }
    BankAccount()
    {
        id = ++NUMBER_OF_ACCOUNTS;
        name = "--";
        balance = 0;
        interest_rate = 0.05;
    }

    void deposit(double amount)
    {
        balance += amount;
        updateLastTransaction(DEPOSIT, *this, amount);
    }

    // return true if amount is less than balance
    bool withdraw(double amount)
    {
        if (amount > balance)
        {
            cout << endl
                 << "\t WITHRAWAL FAILED!  Overdraft protection" << endl
                 << endl;
            return false;
        }
        balance -= amount;
        updateLastTransaction(WITHDRAW, *this, amount);
        return true;
    }

    // return true if amount is less than balance, assumes successful transfer
    bool transfer(BankAccount &toAccount, double amount)
    {
        if (amount > balance)
        {
            cout << endl
                 << "\t TRANSFER FAILED!  Overdraft protection" << endl
                 << endl;
            return false;
        }
        withdraw(amount);
        toAccount.deposit(amount);
        updateLastTransaction(TRANSFER, toAccount, amount);
        return true;
    }

    void display(bool isMenu = false)
    {
        if (isMenu)
        {
            cout << " " << id << "\t" << name << endl;
        }
        else
        {
            cout << " " << id << "\t" << name << "\t";
            if (name.size() < 8) cout << "\t"; //TODO Fix layout
            cout << "$" << balance << endl;
        }
    }

    void setName(string account_name)
    {
        name = account_name;
    }

    void setRate(double rate)
    {
        interest_rate = rate;
    }

    int getId()
    {
        return id;
    }

    string getName()
    {
        return name;
    }

    double getBalance()
    {
        return balance;
    }

    double getRate()
    {
        return interest_rate;
    }

    Transaction getLastTransaction()
    {
        return lastTransaction;
    }

private:
    int id;
    string name;
    double balance;
    double interest_rate;
    Transaction lastTransaction;

    void updateLastTransaction(TransactionType type, BankAccount toAccount, double amount)
    {
        Transaction thisTransaction;
        thisTransaction.type = type;
        thisTransaction.amount = amount;
        thisTransaction.from_id = id;
        thisTransaction.to_id = toAccount.getId();
        lastTransaction = thisTransaction;
    }
};

/*-----------------------------------------------------------------------------
  TODO: BankAccountManager Class  */

vector<BankAccount> manageAccounts(vector<BankAccount> &accountList);

void addAccount(vector<BankAccount> &accountList);

void deposit(vector<BankAccount> &accountList);

void withdraw(vector<BankAccount> &accountList);

void transfer(vector<BankAccount> &accountList);

void listAccounts(vector<BankAccount> accountList, bool isMenu = false);

void writeToCSV(vector<BankAccount> accountList, string directory);

//-----------------------------------------------------------------------------

int main()
{
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);
    //--------------------------------------------------------
    BankAccount checking("Checking", 1000);
    BankAccount savings("Savings");
    //--------------------------------------------------------
    if (DEV_MODE)
    {
        checking.display();
        savings.display();
        cout << "--------------------------------------------" << endl;
        checking.withdraw(40);
        Transaction lastTransaction = checking.getLastTransaction();
        cout << "After checking withdrawal of $" << lastTransaction.amount << ": " << endl;
        checking.display();
        savings.display();
        cout << "--------------------------------------------" << endl;
        cout << "Transfer $60 to " << savings.getName() << " account. " << endl;
        if (checking.transfer(savings, 60))
        {
            Transaction lastDeposit = checking.getLastTransaction();
            cout << "| Account ID:  " << lastDeposit.from_id
                 << " has made a deposit of $" << lastDeposit.amount << endl;
        }
        checking.display();
        savings.display();
        //--------------------------------------------------------
        cout << "Press enter to continue ..." << endl;
        cin.get();
    }
    else
    {
        vector<BankAccount> accountsList = { checking, savings };
        accountsList = manageAccounts(accountsList);
    }
    //------------------------------------------------------------
    return 0;
}

vector<BankAccount> manageAccounts(vector<BankAccount> &accountList)
{
    char users_selection;
    bool done = false;
    while (!done)
    {
        cout << " Select a choice: (L)ist, (N)ew, (D)eposit, (W)ithdraw, (T)ransfer, (S)ave, (Q)uit:  ";
        cin >> users_selection;
//        try
//        {
//            users_selection = getValidatedInput<char>();
//        }
//        catch (exception e)
//        {
//            cerr << e.what() << endl;
//            continue;
//        }
        switch (users_selection)
        {
            case 'L': case 'l':
                listAccounts(accountList);
                break;
            case 'N': case 'n':
                cout << endl;
                addAccount(accountList);
                listAccounts(accountList);
                break;
            case 'D': case 'd':
                listAccounts(accountList, true); //isMenu=true
                deposit(accountList);
                break;
            case 'W': case 'w':
                listAccounts(accountList, true); //isMenu=true
                withdraw(accountList);
                break;
            case 'T': case 't':
                listAccounts(accountList, true); //isMenu=true
                transfer(accountList);
                cout << endl;
                break;
            case 'S': case 's':
                writeToCSV(accountList, "c:\\cs131_temp\\");
                break;
            case 'Q': case 'q':
                done = true;
                break;
            default:
                cout << "Invalid entry, please enter a listed selection." << endl;
                break;
        }
    }
    return accountList;
}

void addAccount(vector<BankAccount> &accountList)
{
    BankAccount account;
    string name;
    cout << " Enter an account name: ";
    cin.ignore();       //clean input stream
    getline(cin, name); //capture proper names
    account.setName(name);

    double balance;
    cout << " Enter the starting balance: ";
    cin >> balance;
    account.deposit(balance);

    accountList.push_back(account);
}

void listAccounts(vector<BankAccount> accountList, bool isMenu)
{
    cout << endl;
    if (isMenu)
    {
        cout << " Acc.#" << "\t" << "Name" << endl
             << "-----------------------------------" << endl;
        for (BankAccount account : accountList)
        {
            account.display(true); //isMenu=true
        }
    }
    else
    {
        cout << " Acc.#" << "\t" << "Name" << "\t\t" << "Balance" << endl
             << "-----------------------------------" << endl;
        for (BankAccount account : accountList)
        {
            account.display();
        }
    }
    cout << endl;
}

void deposit(vector<BankAccount> &accountList)
{
    double amount_to_deposit = 0;
    int account_number;

    cout << " Enter an account number to withdraw from: ";
    cin >> account_number;

    cout << " Enter the amount to deposit: ";
    cin >> amount_to_deposit;

    accountList[account_number - 1].deposit(amount_to_deposit);
}

void withdraw(vector<BankAccount> &accountList)
{
    double amount_to_withdraw = 0;
    int account_number;

    cout << " Enter an account number to withdraw from: ";
    cin >> account_number;

    cout << " Enter the amount to withdraw: ";
    cin >> amount_to_withdraw;

    accountList[account_number - 1].withdraw(amount_to_withdraw);
}

void transfer(vector<BankAccount> &accountList)
{
    double amount_to_transfer = 0;
    int from_id, to_id;

    cout << " Select an account number to transfer from: ";
    cin >> from_id;

    cout << " Select an account number to transfer to: ";
    cin >> to_id;

    cout << " Enter the amount to transfer: ";
    cin >> amount_to_transfer;

    accountList[from_id-1].transfer(accountList[to_id-1], amount_to_transfer);
}

void writeToCSV(vector<BankAccount> accountList, string directory)
{
    ofstream accountsFile;
    accountsFile.open(directory + "accounts_list.csv");
    if (accountsFile.fail())
    {
        // TODO: log and throw error
        throw exception();
        /*
         * exit(EXIT_FAILURE);
         *
         * exit does not perform any sort of stack unwinding, and
         * no alive object on the stack will call its respective
         * destructor to perform cleanup.*/
    }
    for (BankAccount account : accountList)
    {
        accountsFile << account.getId() << ", "
                     << account.getName() << ", "
                     << account.getBalance() << "\n";
    }
    accountsFile.close();
}