/*
 * Lab 01 - Multiplier
 *
 * Write a C++ program that asks the user for three numbers.
 * Multiply the first two numbers and add the third number
 * to the product of the first two.
 *
 * Student: Brian Teachman (mr.teachman@gmail.com)
 * Date:    7/5/2016
 * Class:   CS& 131: Computer Science I C++
 * */

#include <iostream>

using namespace std;

int main() {

    // allocate memory for three 8-bit integers
    int first_number, second_number, third_number;

    // ask user for three numbers
    cout << "Please enter three numbers." << endl << endl;
    cout << "1. What is your first number? ";
    cin >> first_number; // stored input at reserved memory location
    cout << "2. What is your second number? ";
    cin >> second_number;
    cout << "3. What is your third number? ";
    cin >> third_number;

    // multiply the first two numbers and add the third number to the product of the first two
    int total = first_number * second_number + third_number;

    // stream the output to the standard out.
    cout << "\n\t" << first_number << " times " << second_number << " plus " << third_number;
    cout << " equals " << total << "." << endl << endl;

// check compiler macro
#if ( _MSC_VER ) // if using Visual Studio
    system("pause");
#endif

    return 0;
}