/*
 * Lab 02a - if - Grade Translator
 *
 * Write a C++ program that asks the user for a test score. Use a
 * series of if and else if statements that compare the score to
 * the standard grading scale of >= 90 for an A, 80-89 for a B, etc.
 *
 * Student: Brian Teachman (mr.teachman@gmail.com)
 * Date:    7/6/2016
 * Class:   CS& 131: Computer Science I C++
 * */

#include <iostream>

using namespace std;

int main() {

    // allocate memory for three 8-bit integers
    int test_score;

    // ask user for three numbers
    cout << endl << "What was your score on that test? ";
    cin >> test_score;

    if (test_score <= 100 & test_score >= 90)
        cout << endl << "Superior!" << endl;
    else if (test_score < 90 && test_score >= 80)
        cout << endl << "\tA 'B'! Not bad." << endl;
    else if (test_score < 80 && test_score >= 70)
        cout << endl << "\tC huh, go ahead and catch up on your rest. :P" << endl;
    else if (test_score < 80 && test_score >= 70)
        cout << endl << "\tD ..." << endl;
    else
        cout << endl << "\tYou might want to look that over." << endl;

    // check compiler macro
#if ( _MSC_VER ) // if using Visual Studio
    system("pause");
#endif

    return 0;
}
