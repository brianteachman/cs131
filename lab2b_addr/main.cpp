/*
 * Lab 02b - while - Adder
 *
 * Write a C++ program that uses a while or do-while loop to ask
 * the for numbers to add, continuing to add numbers until the
 * user enters a 0 or uses some other mechanism to stop the loop.
 * At the end of the program print the total.
 *
 * Student: Brian Teachman (mr.teachman@gmail.com)
 * Date:    7/7/2016
 * Class:   CS& 131: Computer Science I C++
 * */

#include <iostream>

using namespace std;

int main() {
    bool is_done = false; // no more user input
    int input=0, sum=0;

    while(true)
    {
        cout << "Enter a number to add to the sum: (Enter 0 to calculate) ";
        cin >> input;

        if (input == 0) is_done = true;

        if(is_done) {
            cout << "The total is " << sum;
            return 0;
        }

        sum += input;
    }
}