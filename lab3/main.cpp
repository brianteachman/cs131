/*
* Lab 3 - Guess a Number Game
*
* Student: Brian Teachman (mr.teachman@gmail.com)
* Date:    7/21/2016
* Class:   CS& 131: Computer Science I C++
* */

// include "stdafx.h" /* uncomment include if using Visual Studio */
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <cmath>
#include <limits>

using namespace std;

int getSeededRand(int maxCount);
// Seeded random number generator
// int maxCount

int getMaxCount(int numberOfGuesses);
// MAX = static_cast<int>(ceil( numberOfGuesses * 3.3 ))

int getUsersGuess(int maxCount);
// int guessCount, maxCount

bool checkUsersGuess(int, int, int);
// int numberGuessed, numberToGuess, guessCount

int playMenu(int maxCount, bool& clean);
// int maxCount, bool& clean
// Obviously changing clean is a side effect

char playAgainMenu(void);
// Returns a validated lowercase character 'y' or 'n', or prompt the user to try again

void toLowercase(basic_string<char>& s);
// as expected ...

void toProperNoun(basic_string<char>& s);
// converts "brian" to "Brian"

int main()
{
    int numberToGuess, numberGuessed, numberOfGuesses;
    bool playAnotherRound = true;
    bool isGuessed = false;

    while (playAnotherRound) // run game loop
    {
        cout << "How many guesses would you like? ";
        cin >> numberOfGuesses;

        // set max number to guess and the actual number to guess
        int maxCount = getMaxCount(numberOfGuesses);
        numberToGuess = getSeededRand(maxCount);

        int i = 0;
        while (i < numberOfGuesses && !isGuessed) // guess a number game loop
        {
            numberGuessed = getUsersGuess(maxCount);
            isGuessed = checkUsersGuess(numberGuessed, numberToGuess, i);
            i++;
        }

        if (playAgainMenu() == 'y') {
            isGuessed = false; // reset for next round
            playAnotherRound = true;
        }
        else {
            playAnotherRound = false; // end this gaming session
        }
    }
    return 0;
}

int playMenu(int maxCount, bool& clean) {
    int input;
    cout << "Choose a number between 1 and " << maxCount << ": ";
    cin >> input;
    while (cin.fail()) // <cite> START:
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cout << endl << "Look, I said a N.U.M.B.E.R. between 1-" << maxCount << "! " << endl << endl;
        cin >> input;
    } // END <cite> http://stackoverflow.com/a/19018335
    if (input > 0 && input <= maxCount) {
        clean = true;
    }
    else {
        cout << endl << "Ah hem, I said a number between 1-" << maxCount << " ..." << endl << endl;
    }
    return input;
}

char playAgainMenu() {
    char response;
    bool clean = false;
    while (!clean)
    {
        cout << "Play again? (y|n): ";
        cin >> response;
        switch (response) {
            case 'y': case 'Y':
                clean = true;
                break;
            case 'n': case 'N':
                clean = true;
                break;
            default:
                cout << "Invalid entry; please try again";
                break;
        }
    }
    tolower(response);
    return response;
}

int getSeededRand(int maxCount)
{
    srand( static_cast<unsigned int>( time(NULL) ));
    return rand() % maxCount + 1; // <-- will never be zero (remainder plus one)
}

int getMaxCount(int numberOfGuesses) {
    return static_cast<int>(ceil(numberOfGuesses * 3.3));
}

int getUsersGuess(int maxCount) {
    int usersGuess;
    bool clean = false;
    while (!clean)
    {
        usersGuess = playMenu(maxCount, clean); // sets clean as a side effect
    }
    return usersGuess;
}

bool checkUsersGuess(int usersGuess, int theNumber, int guessCount) {
    bool isGuessed = false;

    if (usersGuess > theNumber) {
        cout << endl << "\t" << "Lower" << endl;
    }
    else if (usersGuess < theNumber) {
        cout << endl << "\t" << "Higher" << endl;
    }
    else if (usersGuess == theNumber) {
        cout << endl << "\t";
        if (guessCount == 0) cout << "Wow, first try!!! ";
        cout << "Way to go, you got it!" << endl;
        isGuessed = true;
    }
    else {
        cout << "Invalid entry; please try again";
    }
    cout << endl;
    return isGuessed;
}

void toProperNoun(basic_string<char>& s) {
    bool firstChar = true;
    for (basic_string<char>::iterator p = s.begin(); p != s.end(); ++p) {
        if (firstChar) {
            *p = toupper(*p);
            firstChar = false;
        }
    }
}

void toLowercase(basic_string<char>& s) {
    for (basic_string<char>::iterator p = s.begin(); p != s.end(); ++p) {
        *p = tolower(*p);
    }
}
